﻿using WebsocketSample.Backend.Utils;

namespace WebsocketSample.Backend.Models
{
    public class Player
    {
        public string UserName { get; set; }

        public EventDrivenWebSocket EventDrivenWebSocket { get; set; }
    }
}
