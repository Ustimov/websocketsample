﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WebsocketSample.Backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var url = "http://localost:5000/";
            if (args.Length == 1)
            {
                url = args[0];
            }

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseUrls(url)
                .Build();

            host.Run();
        }
    }
}
