﻿using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using WebsocketSample.Backend.Models;
using WebsocketSample.Backend.Utils;

namespace WebsocketSample.Backend.Game
{
    public class Game
    {
        private ConcurrentQueue<Player> _players = new ConcurrentQueue<Player>();

        private Timer _timer = new Timer(1000 * 60); // One minute

        private Player _markedPlayer;

        public Game()
        {
            _timer.Elapsed += Timer_Elapsed;
        }

        private async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();

            // It's time to choose the looser
            Player player = _markedPlayer;

            var msg = new Message {Type = MessageType.Lost, Payload = string.Empty};
            await player.EventDrivenWebSocket.SendMessage(JsonConvert.SerializeObject(msg));

            await SendBroadcast($"{player.UserName} lost");
        }

        public void RegisterPlayer(EventDrivenWebSocket eventDrivenWebSocket)
        {
            eventDrivenWebSocket.ReceivedData += EventDrivenWebSocket_ReceivedData;

            _players.Enqueue(new Player {EventDrivenWebSocket = eventDrivenWebSocket});
        }

        private async void EventDrivenWebSocket_ReceivedData(object sender, string msg)
        {
            var message = JsonConvert.DeserializeObject<Message>(msg);
            EventDrivenWebSocket eventDrivemWebSocket = (EventDrivenWebSocket)sender;
            switch (message.Type)
            {
                case MessageType.Connect:
                    
                    SetPlayerName(eventDrivemWebSocket, message.Payload);
                    await eventDrivemWebSocket.SendMessage(msg);
                    await SendBroadcast($"User {message.Payload} connected");
                    break;
                case MessageType.Disconnect:
                    await eventDrivemWebSocket.SendMessage(msg);
                    await SendBroadcast($"User {message.Payload} disconnected");
                    break;
                case MessageType.PassTurn:
                    if (_timer.Enabled)
                    {
                        await SendTokenToNextPlayer();
                    }
                    break;
            }
        }

        private void SetPlayerName(EventDrivenWebSocket eventDrivenWebSocket, string name)
        {
            foreach (var player in _players)
            {
                if (player.EventDrivenWebSocket == eventDrivenWebSocket)
                {
                    player.UserName = name;
                    break;
                }
            }
        }

        private async Task SendBroadcast(string text)
        {
            var msg = new Message {Type = MessageType.Log, Payload = text};
            foreach (var player in _players)
            {
                if (player.EventDrivenWebSocket.WebSocket.State == WebSocketState.Open)
                {
                    await player.EventDrivenWebSocket.SendMessage(JsonConvert.SerializeObject(msg));
                }
            }
        }

        public async Task<string> Start()
        {
            if (_timer.Enabled)
            {
                return "Game already started";
            }

            if (_players.Count > 1)
            {
                _timer.Start();
                await SendBroadcast("Game started!");
                await SendTokenToNextPlayer();
                return "Game started!";
            }
            return "Need more players :(";
        }

        private async Task SendTokenToNextPlayer()
        {
            Player player;
            while (!_players.TryDequeue(out player) && player.EventDrivenWebSocket.WebSocket.State != WebSocketState.Open)
            {
                // Do nothing
            };

            var msg = new Message {Type = MessageType.Turn, Payload = string.Empty};
            await player.EventDrivenWebSocket.SendMessage(JsonConvert.SerializeObject(msg));

            _markedPlayer = player;
            _players.Enqueue(player);
        }
    }
}
