﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebsocketSample.Backend.Utils
{
    public class EventDrivenWebSocket
    {
        public event EventHandler<string> ReceivedData;

        public WebSocket WebSocket { get; private set; }

        public EventDrivenWebSocket(WebSocket webSocket)
        {
            WebSocket = webSocket;
        }

        public async Task Listen()
        {
            while (WebSocket.State == WebSocketState.Open)
            {
                var buffer = new ArraySegment<byte>(new byte[1024]);
                var result = await WebSocket.ReceiveAsync(buffer, CancellationToken.None);
                ReceivedData?.Invoke(this, Encoding.UTF8.GetString(buffer.Array, 0, result.Count));
            }
        }

        public async Task SendMessage(string text)
        {
            var msg = new ArraySegment<byte>(Encoding.UTF8.GetBytes(text));
            await WebSocket.SendAsync(msg, WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}
