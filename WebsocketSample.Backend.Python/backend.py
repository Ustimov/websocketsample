from json import dumps, loads
from threading import Timer, Thread
from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory


MESSAGE_TYPE = {
    "Undefined": 0,
    "Connect": 1,
    "Disconnect": 2,
    "Turn": 3,
    "PassTurn": 4,
    "Win": 5,
    "Lost": 6,
    "Log": 7,
    "Error": 8,
}


class Player:

    def __init__(self, websocket):
        self.websocket = websocket
        

class Game:

    def __init__(self):
        self.players = []
        self.markedPlayer = None
        self.isGameStarted = False
    
    def registerPlayer(self, player):
        self.players.append(player)

    def sendTokenToNextPlayer(self):
        if not self.isGameStarted:
            return
        player = self.players.pop(0)
        player.websocket.sendMessage(dumps({"Type": MESSAGE_TYPE["Turn"]}).encode("utf-8"))
        if self.markedPlayer:
            self.players.append(self.markedPlayer)
        self.markedPlayer = player

    def gameEnded(self):
        self.isGameStarted = False
        self.sendBroadcast("{0} lost".format(self.markedPlayer.name))
        self.markedPlayer.websocket.sendMessage(dumps({"Type": MESSAGE_TYPE["Lost"]}).encode("utf-8"))

    def sendBroadcast(self, payload):
        for player in self.players:
            player.websocket.sendMessage(dumps({"Type": MESSAGE_TYPE["Log"], "Payload": payload}).encode("utf-8"))

    def start(self):                             
        timer = Timer(30, self.gameEnded)
        timer.start()

        self.isGameStarted = True
        self.sendTokenToNextPlayer()


game = Game()


class MyServerProtocol(WebSocketServerProtocol):
    
    def onConnect(self, request):
        print("Client connecting: {0}".format(request.peer))
        self.player = Player(self)
        game.registerPlayer(self.player)

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        msgJson = payload.decode("utf8")
        print("Text message received: {0}".format(msgJson))
        msg = loads(msgJson)
        if msg["Type"] == MESSAGE_TYPE["Connect"]:
            self.player.name = msg["Payload"]
            game.sendBroadcast("Player {0} connected".format(self.player.name))
        elif msg["Type"] == MESSAGE_TYPE["PassTurn"]:
            game.sendTokenToNextPlayer()

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))


def commandReader():
    while True:
        s = input("Type \"s\" to start game:\n")
        if s == "s":
            game.start()


if __name__ == '__main__':
    import asyncio

    factory = WebSocketServerFactory("ws://127.0.0.1:9000")
    factory.protocol = MyServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, '0.0.0.0', 9000)
    server = loop.run_until_complete(coro)

    thread = Thread(target=commandReader)
    thread.start()

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
