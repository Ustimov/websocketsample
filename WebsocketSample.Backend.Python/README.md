# Python Backend

Python backend for the Hot Potato game.

## Getting Started

For Windows and Visual Studio:
* Python 3.4 or 3.5 is required (PTVS 2.2.6 doesn't support Python 3.6)
* Install latest [PTVS](https://github.com/Microsoft/PTVS/releases) (2.2.6 for now)
* Open WebsocketSample.sln
* Right mouse button click on Python Environments in WebsocketSample.Backend.Python project and select Add Virtual Environment...
* Set project as startup and run

For Windows and CMD:
* Open WebsocketSample.Backend.Python in CMD
* Run:
```
python -m venv .
Scripts\activate.bat
python -m pip install -r requirements.txt
python backend.py
```

For Linux or macOS:
* Open WebsocketSample.Backend.Python in Terminal
* Run:
```
python -m venv .
source bin/activate
python -m pip install -r requirements.txt
python backend.py
```