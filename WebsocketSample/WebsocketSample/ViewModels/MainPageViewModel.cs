﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using WebsocketSample.Models;
using WebsocketSample.Services;
using Xamarin.Forms;

namespace WebsocketSample.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private readonly Page _page;

        private readonly IGameService _gameService;

        private bool _isMyTurn = false;

        private bool _isConnected = false;

        private Color _buttonColor = Color.Gray;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<string> Log { get; private set; } = new ObservableCollection<string>();

        public ICommand ConnectCommand { get; private set; }

        public ICommand DisconnectCommand { get; private set; }

        public ICommand SendCommand { get; private set; }

        //public Color ButtonColor
        //{
        //    get { return _buttonColor; }
        //    private set
        //    {
        //        _buttonColor = value;
        //        OnPropertyChanged();
        //    }
        //}

        public string UserName { get; set; } = string.Empty;

        public bool IsMyTurn
        {
            get { return _isMyTurn; }
            private set
            {
                _isMyTurn = value;
                OnPropertyChanged();
            }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                _isConnected = value;
                OnPropertyChanged();
            }
        }

        public MainPageViewModel(Page page, IGameService gameService)
        {
            _page = page;
            _gameService = gameService;
            _gameService.OnReceive += GameService_OnReceive;

            ConnectCommand = new Command(() => _gameService.Connect(UserName));
            DisconnectCommand = new Command(() => _gameService.Disconnect());
            SendCommand = new Command(() =>
            {
                _gameService.PassTurn();
                IsMyTurn = false;
                //ButtonColor = Color.Gray;
            });
        }

        private void GameService_OnReceive(object sender, Message e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (e.Type)
                {
                    case MessageType.Turn:
                        IsMyTurn = true;
                        //ButtonColor = Color.Lime;
                        break;
                    case MessageType.Win:
                        await _page.DisplayAlert(null, "You Win!", "OK");
                        break;
                    case MessageType.Lost:
                        await _page.DisplayAlert(null, "You Lost :(", "OK");
                        IsMyTurn = false;
                        break;
                    case MessageType.Error:
                        await _page.DisplayAlert("Error", e.Payload, "OK");
                        break;
                    case MessageType.Log:
                        Log.Insert(0, e.Payload);
                        break;
                    case MessageType.Connect:
                        IsConnected = true;
                        break;
                    case MessageType.Disconnect:
                        IsConnected = false;
                        break;
                }
            });
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
