﻿using WebsocketSample.Services;
using WebsocketSample.ViewModels;
using Xamarin.Forms;

namespace WebsocketSample.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            BindingContext = new MainPageViewModel(this, GameService.Instance);
        }
    }
}
