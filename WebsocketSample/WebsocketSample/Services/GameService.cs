﻿using System;
using Newtonsoft.Json;
using Websockets;
using WebsocketSample.Models;

namespace WebsocketSample.Services
{
    public class GameService : IGameService
    {
        private string _url = "ws://10.0.0.33:9000/";
        private IWebSocketConnection _webSocket;
        private string _userName;

        public static GameService Instance = new GameService();

        public event EventHandler<Message> OnReceive;

        protected GameService()
        {
            _webSocket = WebSocketFactory.Create();
            _webSocket.OnOpened += WebSocket_OnOpened;
            _webSocket.OnClosed += WebSocket_OnClosed;
            _webSocket.OnMessage += WebSocket_OnMessage;
            _webSocket.OnError += WebSocket_OnError;
        }

        public void Connect(string userName)
        {
            _userName = userName;
            _webSocket.Open(_url);
        }

        public void Disconnect()
        {
            var msg = new Message {Type = MessageType.Disconnect, Payload = _userName};
            _webSocket.Send(JsonConvert.SerializeObject(msg));
            _webSocket.Close();
        }

        public void PassTurn()
        {
            var msg = new Message {Type = MessageType.PassTurn, Payload = _userName};
            _webSocket.Send(JsonConvert.SerializeObject(msg));
        }

        private void WebSocket_OnError(string error)
        {
            OnReceive?.Invoke(this, new Message {Type = MessageType.Error, Payload = error});
        }

        private void WebSocket_OnMessage(string msg)
        {
            OnReceive?.Invoke(this, JsonConvert.DeserializeObject<Message>(msg));
        }

        private void WebSocket_OnClosed()
        {
            OnReceive?.Invoke(this, new Message {Type = MessageType.Disconnect, Payload = string.Empty});
        }

        private void WebSocket_OnOpened()
        {
            var msg = new Message {Type = MessageType.Connect, Payload = _userName};
            _webSocket.Send(JsonConvert.SerializeObject(msg));
        }
    }
}