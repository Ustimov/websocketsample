﻿using System;
using WebsocketSample.Models;

namespace WebsocketSample.Services
{
    public interface IGameService
    {
        event EventHandler<Message> OnReceive; 
        void PassTurn();
        void Connect(string userName);
        void Disconnect();
    }
}
