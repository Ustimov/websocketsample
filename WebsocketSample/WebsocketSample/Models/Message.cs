﻿namespace WebsocketSample.Models
{
    public class Message
    {
        public MessageType Type { get; set; }

        public string Payload { get; set; }
    }
}
