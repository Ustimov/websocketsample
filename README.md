# Hot Potato

The Hot Potato game for demontration using of websockets in Xamarin.Forms.

## Status

Currently in development.

## Credits

This app uses [Websocket.PCL](https://www.nuget.org/packages/Websockets.Pcl/) library by [Nicholas Ventimiglia](https://github.com/NVentimiglia).

## License

Licensed under MIT.